let searchBtn = document.querySelector('.head-search__open'),
    searchHead = document.querySelector('.head-search'),
    menuBtn = document.querySelector('.menu-btn'),
    menuClose = document.querySelector('.main-menu__close'),
    menu = document.querySelector('.main-menu'),
    openFilter = document.querySelector('.open-filter'),
    closeFilter = document.querySelector('.close-filter'),
    modalClose = document.querySelector('.modal__close'),
    modal = document.querySelector('.modal');
searchBtn.addEventListener('click', function () {
    searchHead.classList.add('head-search--open');
})
menuBtn.addEventListener('click', eventsMenu);
menuClose.addEventListener('click', eventsMenu);
function eventsMenu() {
    document.querySelector('.header').classList.toggle('main-menu--open');
}
if (openFilter) {
    openFilter.addEventListener('click', eventsFilter);
    closeFilter.addEventListener('click', eventsFilter);
}
function eventsFilter() {
    document.querySelector('.filter__con').classList.toggle('filter__con--open');
}
if (document.querySelector('.slider-popular')) {
    const introSlider = new Swiper('.slider-popular', {
        slidesPerView: 6,
        pagination: {
            el: '.slider-info__counter',
            clickable: true,
            type: 'fraction'
        },
        // Navigation arrows
        navigation: {
            nextEl: '.slider-popular--next',
            prevEl: '.slider-popular--prev',
        },
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 5,
            },
            1200: {
                slidesPerView: 6,
            },
            1640: {
                spaceBetween: 30,
            }
        }
    });
}
if (document.querySelector('.slider-before')) {
    const introSlider = new Swiper('.slider-before', {
        slidesPerView: 6,
        pagination: {
            el: '.slider-info__counter',
            clickable: true,
            type: 'fraction'
        },
        // Navigation arrows
        navigation: {
            nextEl: '.slider-before--next',
            prevEl: '.slider-before--prev',
        },
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 5,
            },
            1200: {
                slidesPerView: 6,
            },
            1640: {
                spaceBetween: 30,
            }
        }
    });
}
if (document.querySelector('.slider-index')) {
    const introSlider = new Swiper('.slider__list--index', {
        pagination: {
            el: '.slider-info__counter',
            clickable: true,
            type: 'fraction'
        },
        navigation: {
            nextEl: '.slider-index--next',
            prevEl: '.slider-index--prev',
        },
        loop: false,
        autoHeight: false,
        grid: {
            rows: 2,
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 6,
                spaceBetween: 20,
            },
            1640: {
                slidesPerView: 6,
                spaceBetween: 30,
            }
        }
    });
}
if (document.querySelector('.slider-first')) {
    const introSlider = new Swiper('.first-slider__list', {
        loop: false,
        pagination: false,
        // Navigation arrows
        navigation: {
            nextEl: '.slider-first--next',
            prevEl: '.slider-first--prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 15,
            },
            1200: {
                slidesPerView: 4,
                spaceBetween: 15,
            },
            1640: {
                slidesPerView: 4,
                spaceBetween: 10,
            }
        }
    });
}
if (document.querySelector('.contacts-page__slider') && (document.body.clientWidth < 767)) {
    document.querySelector('.contacts-page__pics-list').classList.add('swiper-wrapper');
    let contactsSlides = document.querySelectorAll('.contacts-page__prev');
    contactsSlides.forEach(function(elem) {
        elem.classList.add('swiper-slide');
    });
    const introSlider = new Swiper('.contacts-page__slider', {
        loop: false,
        pagination: {
            el: '.slider-info__counter',
            clickable: true,
            type: 'fraction'
        },
        navigation: {
            nextEl: '.contacts-slider--next',
            prevEl: '.contacts-slider--prev',
        },
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 1,
            }
        }
    });
}
if (document.querySelector('.open-order')){
    let openOrders = document.querySelectorAll('.open-order');
    openOrders.forEach(function(elem) {
        elem.addEventListener('click', openOrder);
    });
}
if (document.querySelector('.open-help')){
    let openHelps = document.querySelectorAll('.open-help');
    openHelps.forEach(function(elem) {
        elem.addEventListener('click', openHelp);
    });
}
function openOrder(){
    modal.classList.remove('modal--success');
    modal.classList.add('modal--order');
    document.querySelector('.content').classList.add('content--modal-open');
}
function openHelp(){
    modal.classList.remove('modal--success');
    modal.classList.add('modal--help');
    document.querySelector('.content').classList.add('content--modal-open');
}
modalClose.addEventListener('click', function(){
    document.querySelector('.content').classList.remove('content--modal-open');
    modal.classList.remove('modal--order');
    modal.classList.remove('modal--help');
})
// при успешной отправке формы
function successForm(){
    modal.classList.add('modal--success');
}
if (document.querySelector('.quant')){
    let quantPlusBtns = document.querySelectorAll('.quant__btn--plus');
    quantPlusBtns.forEach(function(elem) {
        elem.addEventListener('click', quntPlus);
    });
    let quantMinusBtns = document.querySelectorAll('.quant__btn--minus');
    quantMinusBtns.forEach(function(elem) {
        elem.addEventListener('click', quntMinus);
    });
    // quant__btn--minus
    // quant__btn--plus
    // quant__number
}
function quntMinus(){
    console.log(this.nextElementSibling.value);
    let number = parseInt(this.nextElementSibling.value);
    if (number !== 1){
        number =number - 1;
        this.nextElementSibling.value = number;
    }
};
function quntPlus(){
    console.log(this.previousElementSibling.value);
    let number = parseInt(this.previousElementSibling.value);
    console.log(number);
    number =number + 1;
    this.previousElementSibling.value = number;
};

if( document.querySelector('.switch-type__btn')){
    let switchs = document.querySelectorAll('.switch-type__btn');
    switchs.forEach(function(elem) {
        elem.addEventListener('click', switchBtn);
    });
}
function switchBtn(){
    let switchs = document.querySelectorAll('.switch-type__btn');
    switchs.forEach(function(elem) {
        elem.classList.toggle('btn--switch-active');
    });
}